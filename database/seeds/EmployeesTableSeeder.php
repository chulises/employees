<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->delete();
            $json = File::get("database/data/db.json");
            $data = json_decode($json);
            foreach ($data as $obj) {
                DB::table('employees')->insert([ 
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'last_name' => $obj->last_name,
                    'email' => $obj->email,
                    'phone' => $obj->phone,
                ]);
        }
    }
}

