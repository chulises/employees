<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Employee;

class Employees extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = strtolower($request->get('searchName'));
        $last_name = strtolower($request->get('searchLast_name'));
        $email = strtolower($request->get('searchEmail'));
        $phone = strtolower($request->get('searchPhone'));

        $employees = DB::table('employees')
        ->where(DB::raw('lower(name)'), 'like', "%$name%")
        ->where(DB::raw('lower(last_name)'), 'like', "%$last_name%")
        ->where(DB::raw('lower(email)'), 'like', "%$email%")
        ->where(DB::raw('lower(phone)'), 'like', "%$phone%")
        ->paginate(5);

        return $employees;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Employee::max('id');
        $id++;
        Employee::create([
            'id' => $id,
            'name' => $request->get('name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone')
        ]);

        $noti = "Se ha registrado el nuevo empleado";

        return $noti;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employees = Employee::findOrFail($id);

        $data = $request->only('name', 'last_name', 'email', 'phone');

        $employees->fill($data);
        $employees->save();
        
        $noti = "Se ha editado correctamente el empleado";

        return $noti;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Employee = Employee::find($id);
        $Employee -> delete();

        $noti = "Se ha eliminado correctamente a el empleado ".$Employee['name']." ".$Employee['last_name'];

        return $noti;
    }
}
