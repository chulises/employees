@extends('layouts.panel')

@section('content')
<div class="card shadow">
    <div class="card-header border-0">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="mb-0">Empleados</h3>
            </div>
            <input type="text" hidden disabled value="1" id="PagAct">
            <div class="col mt-4 mb-3 text-right"  style="margin-right:-.5rem;">
                <span>Página&nbsp;&nbsp;</span>
            </div>
            <div class="col mt-4 mb-3 text-right" style="margin-right:-3rem;">
                <select name="paginas" id="paginas" class="form-control w-auto" onchange="paginaActual(this.selectedIndex+1);limpiar();recargar(this.value);">
                    <option value="">1</option>
                </select> 
            </div>
        </div>
    </div>

    <div id="noti"></div>
    <div class="table-responsive">
        <!-- Projects table -->
        <table class="table table-bordered align-items-center" id="tabla_resultados">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Correo electrónico</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Acciones</th>
                </tr>
                <form action="" id="buscar" method="POST">
                    @csrf
                    <tr>
                        <th scope="col"><input onchange="paginaActual(1);limpiar();recargar('employees?page=1');" onkeydown="paginaActual(1);limpiar();recargar('employees?page=1');" onkeyup="paginaActual(1);limpiar();recargar('employees?page=1');" class="form-control h-auto" style="background: transparent;" type="text" placeholder="Buscar Nombre" name="searchName" id="searchName"></th>
                        <th scope="col"><input onchange="paginaActual(1);limpiar();recargar('employees?page=1');" onkeydown="paginaActual(1);limpiar();recargar('employees?page=1');" onkeyup="paginaActual(1);limpiar();recargar('employees?page=1');" class="form-control h-auto" style="background: transparent;" type="text" placeholder="Buscar Apellido" name="searchLast_name" id="searchLast_name"></th>
                        <th scope="col"><input onchange="paginaActual(1);limpiar();recargar('employees?page=1');" onkeydown="paginaActual(1);limpiar();recargar('employees?page=1');" onkeyup="paginaActual(1);limpiar();recargar('employees?page=1');" class="form-control h-auto" style="background: transparent;" type="text" placeholder="Buscar Correo" name="searchEmail" id="searchEmail"></th>
                        <th scope="col"><input onchange="paginaActual(1);limpiar();recargar('employees?page=1');" onkeydown="paginaActual(1);limpiar();recargar('employees?page=1');" onkeyup="paginaActual(1);limpiar();recargar('employees?page=1');" class="form-control h-auto" style="background: transparent;" type="text" placeholder="Buscar Telefono" name="searchPhone" id="searchPhone"></th>
                        <th scope="col"><span onclick="limpiarBusqueda();" class="btn btn-sm btn-success">Limpiar búsqueda</span></th>
                    </tr>
                </form>
            </thead>
            <tbody>         
            </tbody>
        </table>
        <div class="col text-right mt-4 mb-3">
            <a href="#" class="btn btn-sm btn-success" onclick="limpiar();limpiarCrear();" id="btnCrear" data-toggle="modal" data-target="#create" title="Agregar empleado">
                Nuevo empleado
            </a>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
    $(document).ready(function(){
        recargar("employees?page=1");
    });
    function limpiar() {
        $("#noti").empty();
        $("#error").empty();
        $("#error2").empty();
        $("#error3").empty();
    }
    function limpiarCrear(){
        document.getElementById('nC').value = "";
        document.getElementById('lC').value = "";
        document.getElementById('eC').value = "";
        document.getElementById('pC').value = "";
    }
    function limpiarBusqueda(){
        document.getElementById('searchName').value = "";
        document.getElementById('searchLast_name').value = "";
        document.getElementById('searchEmail').value = "";
        document.getElementById('searchPhone').value = "";
        paginaActual(1);limpiar();recargar('employees?page=1');
    }
    function paginaActual(pos){
        document.getElementById('PagAct').value = pos;
        
    }
    function recargar(urlVar) {
        var info = $('#buscar').serialize();
        //console.log(info);
        $.ajax({
            type: "GET",
            url: urlVar,
            data: info,
            success: function(response) {
                //console.log(response);
                $('#tabla_resultados > tbody').empty();
                for (  i = 0 ; i < response.data.length; i++){ 
                    var nuevafila= "<tr><td style='display:none'>" +
                    response.data[i].id + "</td><td>" +
                    response.data[i].name + "</td><td>" +
                    response.data[i].last_name + "</td><td>" +
                    response.data[i].email + "</td><td>" +
                    response.data[i].phone + "</td><td>" +
                    '<form action="" method="POST">' +
                        '@csrf'                    +
                        '@method("DELETE")'        +
                        '<a onclick="limpiar();" href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#update" title="Editar empleado" id="btnEditar">Editar</a>' +
                        '<a onclick="limpiar();" href="" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete" title="Eliminar empleado" id="btnEliminar">Eliminar</a>' +
                        '</form>'                 +
                    '</td></tr>'
        
                    $("#tabla_resultados").append(nuevafila)
                    
                }
                $("#paginas").empty();
                for (i = 1; i <= response.last_page; i++) {
                    if (document.getElementById('PagAct').value == i) {
                        var pagina = '<option selected id="'+i+'" value="employees?page='+i+'">'+i+'</option>'
                        $("#paginas").append(pagina)
                    } else {
                        var pagina = '<option id="'+i+'" value="employees?page='+i+'">'+i+'</option>'
                        $("#paginas").append(pagina)
                    }
                }
            },
            error: function(error) {
                //console.log(error);
            }
            
        })
    }
</script>

@endsection

@section('modals')
@include('employees.create')
@include('employees.update')
@include('employees.delete')
@endsection