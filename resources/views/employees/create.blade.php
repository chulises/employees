<div class="modal fade" id="create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Nuevo Empleado</h3>
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
            </div>
            <form action="/employees" id="guardar" method="POST">
            @csrf
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="name">Nombre empleado</label>
                            <input type="text" name="name" id="nC" class="form-control" placeholder="Nombre empleado" required maxlength="20">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="last_name">Apellido</label>
                            <input type="text" name="last_name" id="lC" class="form-control" placeholder="Apellido" required maxlength="30">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email">Correo electrónico</label>
                            <input type="email" name="email" id="eC" class="form-control" placeholder="example@email.com" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="phone">Teléfono</label>
                            <input type="tel" name="phone" id="pC"  class="form-control" placeholder="4443332222" required minlength="7" maxlength="13">
                        </div>
                    </div>
                </div>
                <div id="error"></div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Guardar</button> 
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#guardar').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/employees",
            data: $('#guardar').serialize(),
            success: function(response) {
                //console.log(response);
                if (response != null) {
                    var noti = '<div class="card-body"><div class="alert alert-success" role="alert"><strong>'+response+'</strong></div></div>'
                    $("#noti").append(noti)
                }
                $("#create .close").click()
                url = "employees?page="+document.getElementById('PagAct').value
                recargar(url);
            },
            error: function(response) {
                //console.log(response);
                //console.log("error");
                $("#error").empty();
                var error = '<div class="card-body"><div class="alert alert-danger" role="alert"><strong>El correo electrónico ya esta registrado</strong></div></div>'
                $("#error").append(error)
            }
            
        });
    });
</script>