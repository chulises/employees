<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                
                <h3>¿Estás seguro que deseas eliminar el empleado?</h3>
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>

            </div>
            <form id="eliminarEmpleado">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="nameD">Nombre empleado</label>
                            <input style="background: transparent;border: none;" type="text" name="nameD" id="nameD" class="form-control" required disabled maxlength="20">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="last_nameD">Apellido</label>
                            <input style="background: transparent;border: none;" type="text" name="last_nameD" id="last_nameD" class="form-control" required disabled maxlength="30">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="emailD">Correo electrónico</label>
                            <input style="background: transparent;border: none;" type="email" name="emailD" id="emailD" class="form-control" required disabled>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="phone">Teléfono</label>
                            <input style="background: transparent;border: none;" type="tel" name="phoneD" id="phoneD" class="form-control" required disabled minlength="7" maxlength="13">
                        </div>
                    </div>
                    <input hidden type="name" name="Id_EmpleadoD" id="Id_EmpleadoD" class="form-control" disabled style="background:white">
                    <div id="error3"></div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-danger" value="Eliminar">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script>
    $(document).on('click', '#btnEliminar', function(){

        $tr = $(this).closest('tr');
        var data = $tr.children("td").map(function() {
            return $(this).text();
        }).get();

        $('#Id_EmpleadoD').val(data[0].trim());
        $('#nameD').val(data[1].trim());
        $('#last_nameD').val(data[2].trim());
        $('#emailD').val(data[3].trim());
        $('#phoneD').val(data[4].trim());

    });

    $('#eliminarEmpleado').on('submit', function(e){
        e.preventDefault();
        var id = $('#Id_EmpleadoD').val();
        $.ajax({
            type: "DELETE",
            url: "/employees/" + id,
            data: $('#eliminarEmpleado').serialize(),
            success: function(response){
                $("#noti").empty();
                if (response != null) {
                    var noti = '<div class="card-body"><div class="alert alert-success" role="alert"><strong>'+response+'</strong></div></div>'
                    $("#noti").append(noti)
                }
                $("#delete .close").click()
                paginaActual(1);
                recargar("employees?page=1");
            },
            error: function(error) {
                $("#error3").empty();
                var error = '<div class="card-body"><div class="alert alert-danger" role="alert"><strong>Ha ocurrido un error '+error+'</strong></div></div>'
                $("#error3").append(error)
            }
        });
    });
</script>
