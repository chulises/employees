<div class="modal fade" id="update">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Editar Empleado</h3>
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
            </div>
         <form role="modal" action ="/employees" id="editar" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="name">Nombre empleado</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre empleado" required maxlength="20">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="last_name">Apellido</label>
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Apellido" required maxlength="30">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="email">Correo electrónico</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="example@email.com" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="phone">Teléfono</label>
                        <input type="tel" name="phone" id="phone" class="form-control" placeholder="4443332222" required minlength="7" maxlength="13">
                    </div>
                </div>
                <input hidden type="name" name="Id_Empleado" id="Id_Empleado" class="form-control" placeholder="Id" disabled style="background:white">
            </div>
            <div id="error2"></div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="submit">Editar</button> 
            </div>
        </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script>
    $(document).on('click', '#btnEditar', function(){
        $tr = $(this).closest('tr');
        var data = $tr.children("td").map(function() {
            return $(this).text();
        }).get();

            $('#Id_Empleado').val(data[0].trim());
            $('#name').val(data[1].trim());
            $('#last_name').val(data[2].trim());
            $('#email').val(data[3].trim());
            $('#phone').val(data[4].trim());
            
        });

    $('#editar').on('submit', function(e) {
        e.preventDefault();

        var id = $('#Id_Empleado').val();

        $.ajax({
            type: "PUT",
            url: "/employees/"+ id,
            data: $('#editar').serialize(),
            success: function(response) {
                $("#noti").empty();
                if (response != null) {
                    var noti = '<div class="card-body"><div class="alert alert-success" role="alert"><strong>'+response+'</strong></div></div>'
                    $("#noti").append(noti)
                }
                $("#update .close").click()
                url = "employees?page="+document.getElementById('PagAct').value
                recargar(url);
            },
            error: function(error) {
                $("#error2").empty();
                var error = '<div class="card-body"><div class="alert alert-danger" role="alert"><strong>El correo electrónico ya esta registrado por otro usuario</strong></div></div>'
                $("#error2").append(error)
            }
            
        });
    });
</script>